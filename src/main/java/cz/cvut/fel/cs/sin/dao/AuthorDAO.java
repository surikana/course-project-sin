package cz.cvut.fel.cs.sin.dao;

import cz.cvut.fel.cs.sin.entity.Author;

import java.util.List;


public interface AuthorDAO extends DAO<Author> {

    List<Author> findByFirstName(String firstName);

    List<Author> findBySurname(String surname);

    List<Author> findByFullName(String surname, String firstName);

}
