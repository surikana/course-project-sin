package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.entity.Publisher;

import java.util.List;

public interface PublisherService {

	boolean createNewPublisher(Publisher publisher, boolean isTest);

	Publisher find(long id);

	List<Publisher> findAll();
}
