package cz.cvut.fel.cs.sin.controller;

import cz.cvut.fel.cs.sin.entity.Author;
import cz.cvut.fel.cs.sin.entity.Book;
import cz.cvut.fel.cs.sin.entity.Library;
import cz.cvut.fel.cs.sin.entity.Publisher;
import cz.cvut.fel.cs.sin.service.AuthorService;
import cz.cvut.fel.cs.sin.service.BookService;
import cz.cvut.fel.cs.sin.service.LibraryService;
import cz.cvut.fel.cs.sin.service.PublisherService;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

@Model
public class AddBookController {

    @Inject
    Logger logger;

    @Inject
    private BookService addNewBookService;

    @Inject
    private AuthorService authorService;

    @Inject
    private LibraryService libraryService;

    @Produces
    @Named
    private Book newBook;

    @Inject
    private FacesContext facesContext;
    @Inject
    private PublisherService publisherService;

    @PostConstruct
    public void initNewBook() {
        newBook = new Book();
        authors = authorService.findAll();
        if (authors == null || authors.size() == 0) {
            authors = new LinkedList<>();
        }

        publishers = publisherService.findAll();
        if (publishers != null && publishers.size() >= 1) {
            publisher = publishers.get(0);
        } else {
            publishers = new LinkedList<>();
            publisher = null;
        }

        libraries = libraryService.findAll();
        if (libraries != null && libraries.size() >= 1) {
            library = libraries.get(0);
        } else {
            libraries = new LinkedList<>();
            library = null;
        }
    }

    public void addNewBook() {
        boolean res = addNewBookService.createNewBook(newBook, false);

        if (!res) return;

        FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Book added!", "Added successfully");
        facesContext.addMessage(null, m);
        initNewBook();
        logger.info("Book added!");
    }

    private List<Author> authors;

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    private List<Publisher> publishers;
    private Publisher publisher;

    public List<Publisher> getPublishers() {
        return publishers;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    private List<Library> libraries;
    private Library library;

    public List<Library> getLibraries() {
        return libraries;
    }

    public Library getLibrary() {
        return library;
    }
}
