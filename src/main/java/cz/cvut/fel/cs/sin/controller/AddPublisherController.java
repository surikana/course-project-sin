package cz.cvut.fel.cs.sin.controller;

import cz.cvut.fel.cs.sin.entity.Publisher;
import cz.cvut.fel.cs.sin.service.PublisherService;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Logger;

@Model
public class AddPublisherController {

	@Inject
    Logger logger;

	@Inject
	private PublisherService publisherService;

	@Produces
	@Named
	private Publisher newPublisher;

	@Inject
	private FacesContext facesContext;

	@PostConstruct
	public void initNewPublisher(){
		newPublisher = new Publisher();
	}

	public void addNewPublisher(){
		boolean res = publisherService.createNewPublisher(newPublisher, false);
        if (!res) return;

		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Publisher added!", "Added successfully");
		facesContext.addMessage(null, m);
		initNewPublisher();
		logger.info("Publisher added!");
	}
}
