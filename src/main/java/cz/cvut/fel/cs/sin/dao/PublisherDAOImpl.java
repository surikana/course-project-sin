package cz.cvut.fel.cs.sin.dao;

import cz.cvut.fel.cs.sin.entity.Publisher;

import javax.persistence.TypedQuery;
import java.util.List;

public class PublisherDAOImpl extends GenericDAO<Publisher> implements PublisherDAO {
	@Override
	public List<Publisher> findByName(String name) {
		TypedQuery<Publisher> query = em.createNamedQuery("findAuthorByName", Publisher.class).setParameter("name", name);
		return query.getResultList();
	}
}
