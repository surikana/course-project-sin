package cz.cvut.fel.cs.sin.dao;

import cz.cvut.fel.cs.sin.entity.Book;

import java.util.List;

public interface BookDAO extends DAO<Book> {

    List<Book> findByName(String name);

    List<Book> findByGenre(String genre);

    List<Book> findByIsbn(String genre);
}
