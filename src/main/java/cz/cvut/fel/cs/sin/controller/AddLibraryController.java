package cz.cvut.fel.cs.sin.controller;

import cz.cvut.fel.cs.sin.entity.Library;
import cz.cvut.fel.cs.sin.service.LibraryService;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.logging.Logger;

@Model
public class AddLibraryController {

	@Inject
    Logger logger;

	@Inject
	private LibraryService libraryService;

	@Produces
	@Named
	private Library newLibrary;

	@Inject
	private FacesContext facesContext;

	@PostConstruct
	public void initNewLibrary(){
		newLibrary = new Library();
	}

	public void addNewLibrary(){
		boolean res = libraryService.createNewLibrary(newLibrary, false);
        if (!res) return;

		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Library added!", "Added successfully");
		facesContext.addMessage(null, m);
		initNewLibrary();
		logger.info("Library added!");
	}
}
