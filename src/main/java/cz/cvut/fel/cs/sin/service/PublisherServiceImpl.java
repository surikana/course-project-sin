package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.PublisherDAO;
import cz.cvut.fel.cs.sin.entity.Publisher;

import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

@Stateful
public class PublisherServiceImpl implements PublisherService {

	@Inject
	private PublisherDAO publisherDAO;

    @Inject
    private FacesContext facesContext;

	@Override
	public boolean createNewPublisher(Publisher publisher, boolean isTest) {
        boolean isOk = checkConstraints(publisher, isTest);

        if(isOk) {
            publisherDAO.save(publisher);
            return true;
        }

        return false;

	}

	@Override
	public Publisher find(long id) {
		return publisherDAO.find(id);
	}

	@Override
	public List<Publisher> findAll() {
		return publisherDAO.list();
	}

	private boolean checkConstraints(Publisher publisher, boolean isTest) {
        String name = publisher.getName();

        if (name.isEmpty()) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Name cannot be empty!", "");
                facesContext.addMessage(null, m);
            }

            return false;
        }

        if (publisherDAO.findByName(name).size() >= 1) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Name " + name + " already exists!", "");
                facesContext.addMessage(null, m);
            }

            return false;
        }

        return true;
	}
}
