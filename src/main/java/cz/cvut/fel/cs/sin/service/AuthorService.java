package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.entity.Author;

import java.util.List;

public interface AuthorService {

	boolean createNewAuthor(Author author, boolean isTest);

	List<Author> findAll();

	Author find(long id);
}
