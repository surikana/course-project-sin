package cz.cvut.fel.cs.sin.controller;

import cz.cvut.fel.cs.sin.entity.Author;
import cz.cvut.fel.cs.sin.service.AuthorService;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

@Model
public class AddAuthorController {

	@Inject
	private AuthorService addAuthorService;

	@Produces
	@Named
	private Author newAuthor;

	@Inject
	private FacesContext facesContext;

	@PostConstruct
	public void initNewAuthor(){
		newAuthor = new Author();
	}

	public void addNewAuthor(){
        boolean res = addAuthorService.createNewAuthor(newAuthor, false);
        if (!res) return;

		FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Author added!", "Added successfully");
		facesContext.addMessage(null, m);
		initNewAuthor();
	}
}
