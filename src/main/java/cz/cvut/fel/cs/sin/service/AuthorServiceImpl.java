package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.AuthorDAO;
import cz.cvut.fel.cs.sin.entity.Author;

import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

@Stateful
public class AuthorServiceImpl implements AuthorService {

	@Inject
	private AuthorDAO authorDAO;

    @Inject
    private FacesContext facesContext;

	@Override
	public boolean createNewAuthor(Author author, boolean isTest) {
        boolean isOk = checkConstraints(author, isTest);

        if(isOk) {
            authorDAO.save(author);
            return true;
        }

        return false;
	}

	@Override
	public List<Author> findAll() {
		return authorDAO.list();
	}

	@Override
	public Author find(long id) {
		return authorDAO.find(id);
	}

	private boolean checkConstraints(Author author, boolean isTest) {

		if (author.getFirstName().isEmpty()) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "First Name cannot be empty!", "");
                facesContext.addMessage(null, m);
            }

            return false;
        }

		if (author.getLastName().isEmpty()) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Last Name cannot be empty!", "");
                facesContext.addMessage(null, m);
            }

            return false;
        }

        return true;
	}
}
