package cz.cvut.fel.cs.sin.converter;

import cz.cvut.fel.cs.sin.entity.Library;
import cz.cvut.fel.cs.sin.service.LibraryService;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;

@ManagedBean
@RequestScoped
public class LibraryValueConventer implements Converter {

	@Inject
	private LibraryService libraryService;

	@Override
	public Library getAsObject(FacesContext context, UIComponent component, String submittedValue) {
		if (submittedValue == null || submittedValue.isEmpty()) {
			return null;
		}

		try {
            return libraryService.find(Long.valueOf(submittedValue));
		} catch (NumberFormatException e) {
			throw new ConverterException(new FacesMessage(String.format("%s is not a valid Library Id", submittedValue)), e);
		}
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object modelValue) {
		if (modelValue == null) {
			return "";
		}

		if (modelValue instanceof Library) {
			return ((Library) modelValue).getId() + "";
		} else {
			throw new ConverterException(new FacesMessage(String.format("%s is not a valid Library", modelValue)));
		}
	}
}
