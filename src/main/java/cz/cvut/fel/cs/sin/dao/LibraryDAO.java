package cz.cvut.fel.cs.sin.dao;


import cz.cvut.fel.cs.sin.entity.Library;

import java.util.List;

public interface LibraryDAO extends DAO<Library> {

	List<Library> findByName(String name);
}
