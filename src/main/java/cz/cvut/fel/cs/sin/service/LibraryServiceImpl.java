package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.LibraryDAO;
import cz.cvut.fel.cs.sin.entity.Library;

import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;

@Stateful
public class LibraryServiceImpl implements LibraryService {

	@Inject
	private LibraryDAO libraryDAO;

    @Inject
    private FacesContext facesContext;

	@Override
	public boolean createNewLibrary(Library library, boolean isTest) {
        boolean isOk = checkConstraints(library, isTest);

        if(isOk) {
            libraryDAO.save(library);
            return true;
        }

        return false;
	}

	@Override
	public Library find(long id) {
		return libraryDAO.find(id);
	}

	@Override
	public List<Library> findAll() {
		return libraryDAO.list();
	}

	private boolean checkConstraints(Library library, boolean isTest) {

	    String name = library.getName();

        if (name.isEmpty()) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "First Name cannot be empty!", "");
                facesContext.addMessage(null, m);
            }

            return false;
        }

        if (libraryDAO.findByName(name).size() >= 1) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Name " + name + " already exists!", "");
                facesContext.addMessage(null, m);
            }

            return false;
        }

        return true;
	}
}
