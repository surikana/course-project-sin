package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.entity.Book;

import java.util.List;

public interface BookService {

	boolean createNewBook(Book book, boolean isTest);

	List<Book> findAll();

	Book find(long id);
}
