package cz.cvut.fel.cs.sin.service;

public interface AuthorPublisherService {

	boolean signContract(long authorId, long publisherId, boolean isTest);
}
