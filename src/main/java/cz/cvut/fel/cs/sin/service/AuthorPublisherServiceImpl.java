package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.AuthorDAO;
import cz.cvut.fel.cs.sin.dao.PublisherDAO;
import cz.cvut.fel.cs.sin.entity.Author;
import cz.cvut.fel.cs.sin.entity.Publisher;

import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.LinkedList;
import java.util.List;

@Stateful
public class AuthorPublisherServiceImpl implements AuthorPublisherService {

	@Inject
	private AuthorDAO authorDAO;

	@Inject
	private PublisherDAO publisherDAO;

    @Inject
    private FacesContext facesContext;

    private Publisher publisher;
    private Author author;

	@Override
	public boolean signContract(long authorId, long publisherId, boolean isTest) {

        boolean isOk = checkConstraints(authorId, publisherId, isTest);

        if (!isOk) return false;

        List<Author> authors;
        if (publisher.getAuthors() == null) {
            authors = new LinkedList<>();
        } else {
            authors = publisher.getAuthors();
        }

        authors.add(author);
        publisher.setAuthors(authors);
        publisher = publisherDAO.update(publisher);

        List<Publisher> publishers;
        if (author.getPublishers() == null) {
            publishers = new LinkedList<>();
        } else {
            publishers = author.getPublishers();
        }

        publishers.add(publisher);
        author.setPublishers(publishers);
        authorDAO.update(author);

        return true;
	}

    private boolean checkConstraints(long authorId, long publisherId, boolean isTest) {

	    author = authorDAO.find(authorId);
        if (author == null) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Author with id " + authorId + " does not exist.", "");
                facesContext.addMessage(null, m);
            }
            return false;
        }

        publisher = publisherDAO.find(publisherId);
        if (publisher == null) {
            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Publisher with id " + publisherId + " does not exist.", "");
                facesContext.addMessage(null, m);
            }
            return false;
        }

        return true;
    }
}
