package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.entity.Library;

import java.util.List;

public interface LibraryService {

	boolean createNewLibrary(Library library, boolean isTest);

	Library find(long id);

	List<Library> findAll();
}
