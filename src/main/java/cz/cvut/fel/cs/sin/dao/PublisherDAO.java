package cz.cvut.fel.cs.sin.dao;

import cz.cvut.fel.cs.sin.entity.Publisher;

import java.util.List;

public interface PublisherDAO extends DAO<Publisher> {

	List<Publisher> findByName(String name);
}
