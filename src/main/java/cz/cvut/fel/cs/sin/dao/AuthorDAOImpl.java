package cz.cvut.fel.cs.sin.dao;

import cz.cvut.fel.cs.sin.entity.Author;

import javax.persistence.TypedQuery;
import java.util.List;


public class AuthorDAOImpl extends GenericDAO<Author> implements AuthorDAO {

    @Override
    public List<Author> findByFirstName(String firstName) {
        TypedQuery<Author> query =
                em.createNamedQuery("findAuthorByFirstName", Author.class)
                .setParameter("firstName", firstName);
        return query.getResultList();
    }

    @Override
    public List<Author> findBySurname(String lastName) {
        TypedQuery<Author> query =
                em.createNamedQuery("findAuthorByLastName", Author.class)
                        .setParameter(lastName, lastName);
        return query.getResultList();
    }

    @Override
    public List<Author> findByFullName(String lastName, String firstName) {
        TypedQuery<Author> query =
                em.createNamedQuery("findAuthorByFullName", Author.class)
                        .setParameter("firstName", firstName)
                        .setParameter("lastName", lastName);
        return query.getResultList();
    }
}
