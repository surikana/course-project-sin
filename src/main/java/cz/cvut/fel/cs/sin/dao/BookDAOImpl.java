package cz.cvut.fel.cs.sin.dao;

import cz.cvut.fel.cs.sin.entity.Book;

import javax.persistence.TypedQuery;
import java.util.List;

public class BookDAOImpl extends GenericDAO<Book> implements BookDAO {

	@Override
	public List<Book> findByName(String name) {
		TypedQuery<Book> query = em.createNamedQuery("findBookByName", Book.class).setParameter("name", name);
		return query.getResultList();
	}

	@Override
	public List<Book> findByGenre(String genre) {
		TypedQuery<Book> query = em.createNamedQuery("findBookByGenre", Book.class).setParameter("genre", genre);
		return query.getResultList();
	}
	@Override
	public List<Book> findByIsbn(String isbn) {
		TypedQuery<Book> query = em.createNamedQuery("findByIsbn", Book.class).setParameter("isbn", isbn);
		return query.getResultList();
	}
}
