package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.BookDAO;
import cz.cvut.fel.cs.sin.entity.Author;
import cz.cvut.fel.cs.sin.entity.Book;
import cz.cvut.fel.cs.sin.entity.Publisher;

import javax.ejb.Stateful;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import java.util.List;
import java.util.logging.Logger;

@Stateful
public class BookServiceImpl implements BookService {
    @Inject
    private Logger logger;

	@Inject
	private BookDAO bookDAO;

    @Inject
    private FacesContext facesContext;

	@Override
	public boolean createNewBook(Book book, boolean isTest) {
        boolean isOk = checkConstraints(book, isTest);

        if(isOk) {
            logger.info("Book: " + book.toString());
            bookDAO.save(book);
            return true;
        }

        return false;
	}

	@Override
	public List<Book> findAll() {
		return bookDAO.list();
	}

	@Override
	public Book find(long id) {
		return bookDAO.find(id);
	}

	private boolean checkConstraints(Book book, boolean isTest) {

        if (book.getName().equals("")) {
            logger.info("Book has no name");

            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Name cannot be empty!", "");
                facesContext.addMessage(null, m);
            }

            return false;
        }

	    String isbn = book.getIsbn();
		if (isbn.equals("")) {
           logger.info("Book has no isbn");

            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "ISBN cannot be empty!", "");
                facesContext.addMessage(null, m);
            }
            return false;
        }
        else if (!bookDAO.findByIsbn(isbn).isEmpty()) {
            logger.info("Book with isbn: "+bookDAO.findByIsbn(isbn).toString());

            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Book with isbn " + isbn + " already exists!", "");
                facesContext.addMessage(null, m);
            }
            return false;
        }

		if (book.getAuthors() == null || book.getAuthors().size() == 0) {
            logger.info("Book has no authors");

            if(!isTest) {
                FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Book has to have at least one author", "");
                facesContext.addMessage(null, m);
            }
            return false;
        }

        List<Author> authors = book.getAuthors();
        Publisher publisher = book.getPublisher();

        for (Author author: authors) {
            if(!publisher.getAuthors().contains(author)) {
                logger.info("Book author doesnt have contract with publisher");

                if(!isTest) {
                    FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,
                            "Author " + author.getFirstName() + " " + author.getLastName() + " doesn't have contract with " + publisher.getName(), "");
                    facesContext.addMessage(null, m);
                }
                return false;
            }
        }

        return true;
	}
}
