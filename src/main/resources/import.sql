INSERT INTO author (id, firstname, lastname, email) VALUES
  (1, 'Agatha', 'Christie', 'agatha@christie.com'),
  (2, 'Stephen', 'King', 'stephen@king.com'),
  (3 , 'Yo', 'Nesbo', 'yo@nesbo.com');

INSERT INTO library(id, address, name) values
(1, 'Avesome Avenue 33', 'Awesome Library'),
(2, 'Avesome Avenue 35', 'City Library');

INSERT INTO publisher(id, address, name) values
(1, 'Avesome Avenue 33', 'Publishing House'),
(2, 'Avesome Avenue 35', 'Publish Co.');

INSERT INTO book(id, genre, isbn, name, year, library_id, publisher_id) values
(1, 'Horror', '1000', 'Pet Cemetery', 1987, 1, 1),
(2, 'Detective', '1001', 'And Then There Were None', 1989, 2, 2);

insert into book_author(book_id, author_id) values
(1, 2), (2, 1);

insert into publisher_author(publisher_id, author_id) values
(1, 2), (2, 1);

SELECT setval('hibernate_sequence', max(id)) FROM author;
SELECT setval('library_id_seq', max(id)) FROM library;
SELECT setval('publisher_id_seq', max(id)) FROM publisher;
SELECT setval('book_id_seq', max(id)) FROM book;