package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.AuthorDAOImpl;
import cz.cvut.fel.cs.sin.entity.Author;
import cz.cvut.fel.cs.sin.util.Resource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static junit.framework.Assert.assertFalse;

@RunWith(Arquillian.class)
public class AuthorServiceTest {

    @Inject
    private AuthorService authorService;

	@Deployment
	public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Author.class.getPackage())
                .addPackage(AuthorDAOImpl.class.getPackage())
                .addPackage(AuthorService.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

    @Test
    @Transactional
    public void addAuthorWithEmptyFirstName() {
	    Author author = new Author("", "Last Name", "test@test.com", null, null);
        boolean res = authorService.createNewAuthor(author, true);
        assertFalse("Author has to have first name!", res);
	}

    @Test
    @Transactional
    public void addAuthorWithEmptyLastName() {
	    Author author = new Author("First Name", "", "test@test.com", null, null);
        boolean res = authorService.createNewAuthor(author, true);
        assertFalse("Author has to have last name!", res);
	}
}
