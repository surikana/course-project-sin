package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.AuthorDAO;
import cz.cvut.fel.cs.sin.dao.AuthorDAOImpl;
import cz.cvut.fel.cs.sin.dao.PublisherDAO;
import cz.cvut.fel.cs.sin.dao.PublisherDAOImpl;
import cz.cvut.fel.cs.sin.entity.Author;
import cz.cvut.fel.cs.sin.entity.Publisher;
import cz.cvut.fel.cs.sin.util.Resource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertNotNull;

@RunWith(Arquillian.class)
public class AuthorPublisherServiceTest {

	@Inject
	private AuthorPublisherService authorPublisherService;
	@Inject
	private AuthorDAO authorDAO;
	@Inject
	private PublisherDAO publisherDAO;

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Author.class.getPackage())
                .addPackage(AuthorDAOImpl.class.getPackage())
                .addPackage(Publisher.class.getPackage())
                .addPackage(PublisherDAOImpl.class.getPackage())
                .addPackage(AuthorPublisherService.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Transactional
	public void authorSignToPublisher() {

		Author author = new Author();
		author.setFirstName("Test");
		author.setLastName("Test");
		author.setEmail("text@test.com");
		author = authorDAO.save(author);

		Publisher publisher = new Publisher();
		publisher.setName("Test");
		publisherDAO.save(publisher);

		boolean res = authorPublisherService.signContract(author.getId(), publisher.getId(), true);
		assertTrue(res);

		assertNotNull(publisher.getAuthors());
		assertNotNull(author.getPublishers());

		assertEquals(1, publisher.getAuthors().size());
		assertEquals(author.getId(), publisher.getAuthors().get(0).getId());
	}
}
