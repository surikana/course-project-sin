package cz.cvut.fel.cs.sin.service;

import cz.cvut.fel.cs.sin.dao.*;
import cz.cvut.fel.cs.sin.entity.Author;
import cz.cvut.fel.cs.sin.entity.Book;
import cz.cvut.fel.cs.sin.entity.Publisher;
import cz.cvut.fel.cs.sin.util.Resource;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.transaction.api.annotation.Transactional;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import static junit.framework.Assert.*;

@RunWith(Arquillian.class)
public class BookServiceTest {

    @Inject
    private Logger logger;

	@Inject
	private BookService bookService;
	@Inject
	private AuthorDAO authorDAO;
	@Inject
	private PublisherDAO publisherDAO;
	@Inject
	private BookDAO bookDAO;

	@Deployment
	public static WebArchive createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "test.war")
                .addPackage(Book.class.getPackage())
                .addPackage(BookService.class.getPackage())
                .addPackage(BookDAOImpl.class.getPackage())
                .addPackage(Author.class.getPackage())
                .addPackage(AuthorDAOImpl.class.getPackage())
                .addPackage(Publisher.class.getPackage())
                .addPackage(PublisherDAOImpl.class.getPackage())
                .addClass(Resource.class)
                .addAsResource("META-INF/test-persistence.xml", "META-INF/persistence.xml")
                .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

    @Test
    @Transactional
    public void addBookWithEmptyIbsn() {
	    Book book = new Book("", "Test Book", 1961, "horror", null, null, null);
        boolean res = bookService.createNewBook(book, true);
        assertFalse("Book has to have IBSN!", res);
	}

    @Test
    @Transactional
    public void addBookWithEmptyName() {
	    Book book = new Book("test IBSN", "", 1961, "horror", null, null, null);
        boolean res = bookService.createNewBook(book, true);
        assertFalse("Book has to have name!", res);
	}

    @Test
    @Transactional
    public void addBookWithSameIbsn() {
	    Book book = new Book("test IBSN", "", 1961, "horror", null, null, null);
        bookDAO.save(book);

        Book book2 = new Book("test IBSN", "", 1961, "horror", null, null, null);
        boolean res = bookService.createNewBook(book2, true);

        assertFalse("Books shouldn't have same IBSN!", res);

        bookDAO.delete(book.getId());
	}

    @Test
    @Transactional
    public void addBookWithNoAuthors() {
        Book book = new Book("test IBSN", "Test Book", 1961, "horror", null, null, null);
        boolean res = bookService.createNewBook(book, true);
        assertFalse("Book has to have authors!", res);
    }

    @Test
    @Transactional
    public void addBookWithAuthorPublisherCollusion() {
        List<Author> authors = getTestAuthors();
        Publisher publisher = getPublisher(new ArrayList<>());
        Book book = new Book("test IBSN", "Test Book", 1961, "horror", null, publisher, authors);
        boolean res = bookService.createNewBook(book, true);
        assertFalse("Author has to have contract with publisher!", res);
    }

    @Test
    @Transactional
    public void addBook() {
        logger.info("add book test");
        List<Author> authors = getTestAuthors();
        Publisher publisher = getPublisher(authors);
        Book book = new Book("test IBSN", "Test Book", 1961, "horror", null, publisher, authors);
        boolean res = bookService.createNewBook(book, true);
        assertTrue("Book saving failure", res);

        book = bookDAO.save(book);
        List<Book> booksISBN = bookDAO.findByIsbn(book.getIsbn());
        assertEquals("There should be single book with isbn!", booksISBN.size(), 1);

        Book bookFromDB = booksISBN.get(0);

        assertEquals("Book flushing failure", book.getId(), bookFromDB.getId());
    }

    private List<Author> getTestAuthors() {
                List<Author> authors = new ArrayList<>();
        Author author = new Author();
        author.setFirstName("Test");
        author.setLastName("Test");
        author.setEmail("text@test.com");
        author = authorDAO.save(author);
        authors.add(author);
        return authors;
    }

    private Publisher getPublisher(List<Author> authors) {
	    Publisher p = new Publisher();
	    p.setName("Test Publisher");
	    p.setAuthors(authors);
        p = publisherDAO.save(p);

	    return p;
    }
}
